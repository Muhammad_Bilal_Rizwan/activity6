package com.example.activity6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.AsynchronousChannelGroup;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private ListView lv;

    String name, fname, contact, email;

    private static String JSON_URL = "https://run.mocky.io/v3/bee267b3-c903-45f5-a4f8-861c2eee7715";

    ArrayList<HashMap<String,String>> studentsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        studentsList = new ArrayList<>();
        lv = findViewById(R.id.listview);

        GetData getData = new GetData();
        getData.execute();

    }





    public class GetData extends AsyncTask<String, String, String>{


        @Override
        protected String doInBackground(String... strings) {
            String current = "";

            try {
                URL url;
                HttpURLConnection urlConnection = null;

                try {
                    url = new URL(JSON_URL);
                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();
                    InputStreamReader isr = new InputStreamReader(in);

                    int data = isr.read();
                    while(data != -1){

                        current += (char) data;
                        data = isr.read();

                    }
                    return current;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                finally {
                    if(urlConnection != null){
                        urlConnection.disconnect();
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return current;

        }


        @Override
        protected void onPostExecute(String s) {

            try{
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("Students");

                for (int i=0; i<jsonArray.length(); i++){
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    name = jsonObject1.getString("name");
                    fname = jsonObject1.getString("fname");
                    contact = jsonObject1.getString("contact");
                    email = jsonObject1.getString("email");

                    //Hashmap
                    HashMap<String, String> students = new HashMap<>();

                    students.put("name", name);
                    students.put("fname", fname);
                    students.put("contact", contact);
                    students.put("email", email);

                    studentsList.add(students);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            // Displaying the result
            ListAdapter adapter = new SimpleAdapter(
                    MainActivity.this,
                    studentsList,
                    R.layout.row_layout,
                    new String[] {"name", "fname", "contact", "email"},
                    new int[]{R.id.textView, R.id.textView2, R.id.textView3, R.id.textView4});

            lv.setAdapter(adapter);


        }
    }





}

